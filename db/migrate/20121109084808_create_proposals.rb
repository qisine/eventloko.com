class CreateProposals < ActiveRecord::Migration
  def change
    create_table :proposals do |t|
      t.references :event

      t.timestamps
    end
    add_index :proposals, :event_id
  end
end
