class RenameImageToMainImageOnVenue < ActiveRecord::Migration
  def change
    rename_column :venues, :image, :main_image
  end   
end
