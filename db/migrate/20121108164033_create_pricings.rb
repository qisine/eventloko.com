class CreatePricings < ActiveRecord::Migration
  def change
    create_table :pricings do |t|
      t.integer :half_day
      t.integer :full_day
      t.integer :deposit
      t.references :venue

      t.timestamps
    end
    add_index :pricings, :venue_id
  end
end
