class AddDefaultLocaleToCustomer < ActiveRecord::Migration
  def change
    add_column :customers, :default_locale, :string
  end
end
