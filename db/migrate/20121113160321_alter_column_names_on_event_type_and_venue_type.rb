class AlterColumnNamesOnEventTypeAndVenueType < ActiveRecord::Migration
  def change
    rename_column :event_types, :name_en, :short
    rename_column :event_types, :name_de, :locale_file_index
    rename_column :venue_types, :name_en, :short
    rename_column :venue_types, :name_de, :locale_file_index
  end
end
