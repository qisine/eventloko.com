class AlterDateAndTimeOnEvent < ActiveRecord::Migration
  def change
    remove_column :events, :event_date
    remove_column :events, :event_time
    add_column :events, :event_datetime, :datetime
  end
end
