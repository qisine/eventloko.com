class AddFieldsToEvents < ActiveRecord::Migration
  def change
    add_column :events, :start_at, :timestamp
    add_column :events, :end_at, :timestamp
    add_column :events, :all_day, :boolean
    add_column :events, :repeats, :string
  end
end
