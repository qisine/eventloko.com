class AddNameAndLogoToOwner < ActiveRecord::Migration
  def change
    add_column :owners, :name, :string
    add_column :owners, :logo, :string
  end
end
