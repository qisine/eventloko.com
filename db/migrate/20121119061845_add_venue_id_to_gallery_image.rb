class AddVenueIdToGalleryImage < ActiveRecord::Migration
  def change
    remove_column :gallery_images, :imageable_id
    remove_column :gallery_images, :imageable_type
    add_column :gallery_images, :venue_id, :integer
  end
end
