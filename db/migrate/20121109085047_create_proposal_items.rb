class CreateProposalItems < ActiveRecord::Migration
  def change
    create_table :proposal_items do |t|
      t.string :name
      t.text :description
      t.integer :number
      t.integer :price
      t.references :proposal

      t.timestamps
    end
    add_index :proposal_items, :proposal_id
  end
end
