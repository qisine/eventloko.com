class RenameImageMainImageToMainImageId < ActiveRecord::Migration
  def change
    remove_column :venues, :main_image
    add_column :venues, :main_image_id, :integer
  end
end
