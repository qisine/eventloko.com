class AddDefaultVenueToVenue < ActiveRecord::Migration
  def change
    add_column :venues, :default_venue, :boolean
  end
end
