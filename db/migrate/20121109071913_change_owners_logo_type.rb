class ChangeOwnersLogoType < ActiveRecord::Migration
  def change
    change_column :owners, :logo, :text
  end
end
