class AddVenueTypeIdToVenue < ActiveRecord::Migration
  def change
    add_column :venues, :venue_type_id, :integer
  end
end
