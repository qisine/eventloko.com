class RenameImageToGalleryImage < ActiveRecord::Migration
  def change
    rename_table :images, :gallery_images
  end
end
