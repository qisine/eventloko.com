class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.text :name
      t.text :street
      t.string :city
      t.string :zip
      t.text :directions

      t.timestamps
    end
  end
end
