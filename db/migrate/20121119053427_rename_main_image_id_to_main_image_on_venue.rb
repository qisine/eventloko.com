class RenameMainImageIdToMainImageOnVenue < ActiveRecord::Migration
  def change
    remove_column :venues, :main_image_id
    add_column :venues, :main_image, :string
  end
end
