class AddDefaultLocaleToOwner < ActiveRecord::Migration
  def change
    add_column :owners, :default_locale, :string
  end
end
