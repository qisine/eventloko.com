class CreateGeoData < ActiveRecord::Migration
  def change
    create_table :geo_data do |t|
      t.integer :zip
      t.string :city
      t.string :city_transliterated
      t.string :canton

      t.timestamps
    end
  end
end
