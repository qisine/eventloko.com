class CreatePackages < ActiveRecord::Migration
  def change
    create_table :packages do |t|
      t.string :name
      t.string :description
      t.integer :price_per_pax
      t.string :image

      t.timestamps
    end
  end
end
