class AddNameDeToEventType < ActiveRecord::Migration
  def change
    add_column :event_types, :name_de, :string
    rename_column :event_types, :name, :name_en
  end
end
