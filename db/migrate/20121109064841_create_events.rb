class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.date :event_date
      t.time :event_time
      t.integer :pax
      t.integer :duration

      t.timestamps
    end
  end
end
