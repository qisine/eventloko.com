class AddUuidAndCustomerIdToEvent < ActiveRecord::Migration
  def change
    add_column :events, :customer_id, :integer
    add_column :events, :uuid, :string
    add_index :events, :customer_id
  end
end
