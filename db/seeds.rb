require 'csv'

def create_venue_types
  venue_types = IO.read(Rails.root.join('spec', 'fixtures', 'venue_types', "venue_types.txt").to_s).split("\n") 
  venue_types.each do |vt|
    VenueType.create!(locale_file_index: vt)
  end
end

def create_geodata
  geodata = []
  CSV.foreach(Rails.root.join("rel", "geodata", "full_zip_table.csv").to_s, headers: true) do |r|
    geodata << { zip: r["zip"],
                 city: r["city"],
                 city_transliterated: I18n.transliterate(r["city"].to_s),
                 canton: r["canton"] }
  end
  GeoData.create!(geodata)
end

create_venue_types
puts "Venue Types created"
create_geodata
puts "GeoData created"
puts "Seeding done!"
