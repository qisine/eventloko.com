# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121126073554) do

  create_table "addresses", :force => true do |t|
    t.text     "name"
    t.text     "street"
    t.string   "city"
    t.string   "zip"
    t.text     "directions"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.integer  "addressable_id"
    t.string   "addressable_type"
    t.string   "city_transliterated"
  end

  create_table "customers", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "default_locale"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "company"
    t.integer  "owner_id"
  end

  add_index "customers", ["email"], :name => "index_customers_on_email", :unique => true
  add_index "customers", ["reset_password_token"], :name => "index_customers_on_reset_password_token", :unique => true

  create_table "event_types", :force => true do |t|
    t.string   "short"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.string   "locale_file_index"
  end

  create_table "events", :force => true do |t|
    t.integer  "pax"
    t.integer  "duration"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "venue_id"
    t.string   "name"
    t.text     "description"
    t.integer  "customer_id"
    t.string   "uuid"
    t.datetime "event_datetime"
    t.datetime "start_at"
    t.datetime "end_at"
    t.boolean  "all_day"
    t.string   "repeats"
  end

  add_index "events", ["customer_id"], :name => "index_events_on_customer_id"
  add_index "events", ["venue_id"], :name => "index_events_on_venue_id"

  create_table "gallery_images", :force => true do |t|
    t.text     "image"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "venue_id"
  end

  create_table "geo_data", :force => true do |t|
    t.integer  "zip"
    t.string   "city"
    t.string   "city_transliterated"
    t.string   "canton"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "owners", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "name"
    t.text     "logo"
    t.string   "default_locale"
    t.string   "phone"
  end

  add_index "owners", ["email"], :name => "index_owners_on_email", :unique => true
  add_index "owners", ["reset_password_token"], :name => "index_owners_on_reset_password_token", :unique => true

  create_table "packages", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "price_per_pax"
    t.string   "image"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "pricings", :force => true do |t|
    t.integer  "half_day"
    t.integer  "full_day"
    t.integer  "deposit"
    t.integer  "venue_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "pricings", ["venue_id"], :name => "index_pricings_on_venue_id"

  create_table "proposal_items", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "number"
    t.integer  "price"
    t.integer  "proposal_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "proposal_items", ["proposal_id"], :name => "index_proposal_items_on_proposal_id"

  create_table "proposals", :force => true do |t|
    t.integer  "event_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "proposals", ["event_id"], :name => "index_proposals_on_event_id"

  create_table "venue_types", :force => true do |t|
    t.string   "short"
    t.string   "locale_file_index"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "venues", :force => true do |t|
    t.string   "name"
    t.integer  "capacity"
    t.integer  "size"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "owner_id"
    t.boolean  "default_venue"
    t.text     "description"
    t.integer  "venue_type_id"
    t.string   "main_image"
  end

  add_index "venues", ["owner_id"], :name => "index_venues_on_owner_id"

end
