unless Object.const_defined?('FactoryGirl')
  require 'factory_girl'
  require 'faker'
  require Rails.root.join('spec', 'factories.rb')
end

def start_producing
  10.times.each do
    o = FactoryGirl.create(:owner) 
    create_customers(o)
    create_venues(o)
  end
end

def create_customers(owner)
  Random.new.rand(1..10).times.each do
    FactoryGirl.create(:customer, owner: owner)
  end
end

def create_venues(owner)
  Random.new.rand(1..10).times.each do
     v = FactoryGirl.create(:venue, owner_id: owner.id)
     v.save!
  end
end

start_producing

puts "Test data created!"
