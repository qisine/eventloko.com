$(function() {
  var input = $('#search_location')[0];
  if(!input) return;
  var options = {
    types: ['(regions)'],
    componentRestrictions: {country: 'ch'}
  };
  autocomplete = new google.maps.places.Autocomplete(input, options); 
});
