$(function() {
  $(".enquiry_link").on("click", function(e) {
    e.preventDefault();
    var a = $("#contact_form form")
      .attr("action")
      .replace(/\/?\d*$/, '')
      .concat("/", $(this).attr("data-venue-id"));
    $("#contact_form form")
      .attr("action", a)
      .children(".flash")
      .remove();
    $("#contact_form").dialog({
      resizable: false,
      width: 'auto',
      height: 'auto',
      modal: true
    });
  });
});
