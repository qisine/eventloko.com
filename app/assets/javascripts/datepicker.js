$(function() {
  locale = $("html").attr("lang");
  $.datepicker.setDefaults($.datepicker.regional[locale === "de" ? "de" : ""]); 
  $(".datepicker")
    .datepicker("option", "dateFormat", locale === "de" ? "dd.mm.yy" : "dd-mm-yy");
});
