$(function() {
  function showModal(e) {
    e.preventDefault();

    var desc = $(this).children("img").attr("alt");
    var img = "<img src='" + $(this).attr("href") + "' alt='" + desc + "' >";
    $("#venue-details-modal-container")
      .empty()
      .append(img)
      .append("<p>" + desc + "</p>")
      .dialog({
        resizable: false,
        width: 'auto',
        height: 'auto',
        modal: true
      });
  }

  $(".image-link").on("click", showModal);
});
