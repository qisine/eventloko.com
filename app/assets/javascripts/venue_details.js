$(function() {
  $(".tabs").tabs({
    create: function(e, ui) {
      $.getJSON($("#venue_details").data("fetch-url"), function(data) {
        $(".calendar").datepicker({
          beforeShowDay: function(date) {
            var str_date = $.datepicker.formatDate("yy-mm-dd", date);
            var today = $.datepicker.formatDate("yy-mm-dd", new Date());
            if(data.indexOf(str_date) < 0 && str_date >= today ) {
              return [true, 'available'];
            }
            return [false, 'unavailable'];
          }
        });
      });
    }
  });
});
