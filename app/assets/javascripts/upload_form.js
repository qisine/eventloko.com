$(function() {
  $(".edit-images-link").on("click", function(e) {
    e.preventDefault();
    $("#upload_form .flash").remove();
    if($("#upload_form table tr").length <= 0) { $("#upload_form #zero_images").show(); }
    $("#upload_form .image-fields").slice(2).remove();
    $("#upload_form .image-fields").find("input[type=text], input[type=file]").val("");
    $("#upload_form").dialog({
      resizable: false,
      width: 'auto',
      height: 'auto',
      modal: true
    });
  });
  
  $("#upload_form #add_images_link").click(function(e) {
    e.preventDefault();
    var new_fields = $("#upload_form form .image-fields")
      .last()
      .clone();
    
    var match = /\[(\d+)\]/.exec(new_fields.find("input").last().attr("name"));
    var id = parseInt(match[1]);
    if(!isNaN(id) && !(typeof id === "undefined")) {
      id = id + 1;
      $($("<div>").append(new_fields)
          .html()
          .replace(/attributes_\d+/g, "attributes_" + id)
          .replace(/attributes\]\[\d+/g, "attributes][" + id))
      .insertAfter($("#upload_form form .image-fields").last());
    }
  });
});
