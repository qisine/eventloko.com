# need to override the json view to return what full_calendar is expecting.
# http://arshaw.com/fullcalendar/docs/event_data/Event_Object/
# TODO: generate time frame recurring json events

json.array!(@once) do |json, a|
  json.id a.id
  json.title a.calendar_info
  json.start a.start_at.iso8601
  json.end a.end_at.iso8601
  json.allDay a.all_day
end

days = (@start .. @end).to_a

@daily.each do |a|
  days.each do |date|
    if date >= a.start_at.to_date
      to = { year: date.year, month: date.month, day: date.day }
      a.start_at = a.start_at.change(to)
      a.end_at = a.end_at.change(to) if a.end_at.present?

      json.child! do |json|
        json.id a.id
        json.title a.calendar_info
        json.start a.start_at.iso8601
        json.end a.end_at.present? ? a.end_at.iso8601 : a.start_at.iso8601
        json.allDay a.all_day
        json.color a.calendar_color
      end
    end
  end
end

@weekly.each do |a|
  days.each do |date|
    if date >= a.start_at.to_date
      if (date - a.start_at.to_date).to_i % 7 == 0
        to = { year: date.year, month: date.month, day: date.day }
        a.start_at = a.start_at.change(to)
        a.end_at = a.end_at.change(to) if a.end_at.present?

        json.child! do |json|
          json.id a.id
          json.title a.calendar_info
          json.start a.start_at.iso8601
          json.end a.end_at.present? ? a.end_at.iso8601 : a.start_at.iso8601
          json.allDay a.all_day
          json.color a.calendar_color
        end
      end
    end
  end
end
