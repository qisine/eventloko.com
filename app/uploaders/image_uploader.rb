# encoding: utf-8

class ImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::RMagick

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
    #"uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{store_prefix}/#{model.id}"
  end

  #def store_prefix
    #model.image.to_s[0, 2]
  #end

  def default_url
    "default.png"
  end

  version :thumb do
    process :resize_to_fit => [250, 250]
  end

  def extension_white_list
    %w(jpg jpeg gif png svg)
  end
end
