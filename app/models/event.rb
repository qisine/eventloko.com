class Event < ActiveRecord::Base
  REPEATS = ["no", "daily", "weekly"] # , "monthly", "yearly"
  COLORS = {
    "no" => "blue",
    "daily" => "red",
    "weekly" => "green"
  }

  attr_accessible :name, :description, :event_datetime, :pax, :uuid, :customer_id, :venue_id,
                  :start_at, :end_at, :repeats, :all_day #, :duration <-- no longer needed

  belongs_to :venue
  belongs_to :customer
  has_one :proposal

  validates :venue, presence: true
  validates :repeats, presence: true, inclusion: { in: REPEATS }
  validates :all_day, inclusion: { in: [true, false] }
  validates :start_at, presence: true
  validates :end_at, presence: true, unless: :all_day

  validate do
    if !all_day && start_at.present? && end_at.present?
      if start_at > end_at
        errors.add(:base, :start_smaller_than_end)
      else
        errors.add(:base, :start_same_date_end) if (end_at - start_at) > 24.hours
      end
    end
  end

  slit = ->(literal){ Arel::Nodes::SqlLiteral.new(literal) }
  
  scope :in, ->(start, finish) {
    where {
      ((repeats == 'no') & (start_at >= start) & (start_at <= finish)) |
      ((repeats.in(['daily', 'weekly'])) & (start_at <= finish))
    }
  }

  scope :repeats_daily, ->(date) {
    where {
      { repeats: 'daily', start_at.lte => date }
    }
  }

  scope :repeats_weekly, ->(date) {
    where {
      { repeats: 'weekly', start_at.lte => date }
    }
  }

  sifter :search_date_sifter do |date|
    start_date = CAST(slit.("events.start_at AS date"))
    day_of_week = extract(slit.('DOW FROM events.start_at'))
    
    ((repeats == 'no') & (start_date == date)) |
    ((repeats == 'daily') & (start_date <= date)) |
    ((repeats == 'weekly') & (day_of_week == date.wday) & (start_date <= date))
  end

  def calendar_info
    t = "#{venue.name}"
    #t << (all_day ? I18n.t('global.calendar.all_day') : '')
    #t << ' (' + I18n.t("global.calendar.repeats.#{repeats}") + ')' unless repeats.blank?
    t
  end

  def calendar_color
    COLORS[repeats.blank? ? "no" : repeats]
  end

  def self.repeats_collection_for_select
    r=Struct.new(:id, :name)
    REPEATS.reduce([]) {|c,e| c << r.new(e, I18n.t("global.calendar.repeats.#{e}")) }
  end
end
