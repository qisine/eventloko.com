class Customer < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessible :first_name, :last_name, :company, :email, :password, :password_confirmation, :remember_me
  
  belongs_to :owner
  has_many :events
  has_one :address, as: :addressable

  def name
    n = "#{first_name} #{last_name}".strip
    n.empty? ? "Anon" : n
  end
end
