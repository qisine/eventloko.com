class EventType < ActiveRecord::Base
==begin
  Corporate Events:
    - Seminar
    - Workshop
    - Business lunch/dinner
    - Networking
    - Training courses
    - Team building
  Social Event:
    - Wedding
    - Themed Party
    - Anniversary Party
    - Family
==end

  attr_accessible :short, :locale_file_index
end
