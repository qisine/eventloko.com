class Proposal < ActiveRecord::Base
  belongs_to :event
  has_many :proposal_items
end
