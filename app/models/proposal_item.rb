class ProposalItem < ActiveRecord::Base
  belongs_to :proposal
  attr_accessible :description, :name, :number, :price
end
