class OwnerAbility
  include CanCan::Ability

  def initialize(owner)
    if owner.persisted?
      can :manage, Owner, id: owner.id
      can :manage, Venue, owner_id: owner.id

      with_options(venue: { owner_id: owner.id }) do |ability|
        ability.can :manage, Event
        ability.can :manage, Pricing
        ability.can :manage, GalleryImage
      end
    end
  end
end
