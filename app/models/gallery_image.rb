class GalleryImage < ActiveRecord::Base
  attr_accessible :description, :image, :venue_id

  belongs_to :venue
  mount_uploader :image, ImageUploader
end
