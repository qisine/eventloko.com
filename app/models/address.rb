class Address < ActiveRecord::Base
  attr_accessible :city, :city_transliterated, :directions, :name, :street, :zip
  
  belongs_to :addressable, polymorphic: true

  before_save do |a|
    a.city_transliterated = I18n.transliterate(a.city.to_s)
  end

  validates :zip, inclusion: { in: GeoData.pluck(:zip).map {|z| z.to_s } }
end
