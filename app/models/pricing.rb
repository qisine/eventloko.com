class Pricing < ActiveRecord::Base
  belongs_to :venue
  attr_accessible :deposit, :full_day, :half_day
end
