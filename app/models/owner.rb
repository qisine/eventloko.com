class Owner < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessible :email, :password, :password_confirmation, :remember_me,
                  :name, :phone, :logo, :address_attributes

  has_many :venues
  has_many :events, through: :venues
  has_many :customers
  has_one :address, as: :addressable
  
  accepts_nested_attributes_for :address

  mount_uploader :logo, ImageUploader

  delegate :can?, :cannot?, :to => :ability
  
  def ability(options = {:reload => false})
    options[:reload] ? _ability : (@ability ||= _ability)
  end
  
  def name
    n = read_attribute(:name)
    n.try { |s| !s.strip.empty?} ? n : "Anon" 
  end

  protected

  def _ability
    OwnerAbility.new(self)
  end
end
