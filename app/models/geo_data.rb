class GeoData < ActiveRecord::Base
  attr_accessible :canton, :city, :city_transliterated, :zip
end
