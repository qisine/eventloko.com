class CustomerAbility
  include CanCan::Ability

  def initialize(customer, current_uuid)
    @customer = customer || Customer.new
    #@current_uuid = current_uuid
    guest_permissions
    customer_permissions if @customer.persisted?
  end

  def guest_permissions
    can :read, [Owner, Event, Venue, GalleryImage, Pricing, VenueType, EventType]
  end

  def customer_permissions
    can :manage, Customer, id: @customer.id
  end
end
