class Venue < ActiveRecord::Base
  attr_accessible :capacity, :name, :description, :size, :default_venue, :main_image,
                  :pricing_attributes, :address_attributes, :gallery_images_attributes,
                  :venue_type_id

  belongs_to :owner
  belongs_to :venue_type
  has_many :events
  has_many :gallery_images
  has_one :address, as: :addressable
  has_one :pricing
  
  mount_uploader :main_image, ImageUploader

  accepts_nested_attributes_for :pricing, :address, :gallery_images

  scope :address_search, lambda { |addr_element|
    # Unicode normalized string (all diacritics removed)
    #normalized_loc = addr_element.try { |a| a.mb_chars.normalize(:kd).gsub(/[^\x00-\x7F]/n, '').to_s }
    addr_element = I18n.transliterate(addr_element.to_s) 
    if addr_element =~ /^(\d{4})/u
      joins(:address).where("addresses.zip = ?", $1)
    elsif addr_element =~ /^([^,]+)/u
      joins(:address).where("addresses.city_transliterated ilike ?", $1)
    else 
       joins(:address)
       .where("addresses.city ilike ?", !addr_element.blank? ? addr_element : "%")
    end
  }

  scope :price_search, lambda { |lo=0, hi=999_999| 
    return scoped if lo.blank? && hi.blank?
    lo = lo.to_i rescue 0
    hi = hi.blank? ? 999_999 : hi.to_i rescue 999_999
    joins(:pricing)
    .where("pricings.full_day BETWEEN ? AND ?", lo, hi)
  }

  scope :date_search, lambda { |date|
    return scoped if (date = date.to_s).blank?
    date = Date.parse date.to_s 
    joins("LEFT JOIN events on events.venue_id = venues.id")
    .where(<<-SQL, date.strftime("%Y-%m-%d"))
            not exists(
            select 1
            from events e
            where e.start_at::date = ?::date
                  and e.venue_id = venues.id)
           SQL
  }

  scope :venue_type_search, lambda { |venue_type_ids|
    return scoped if venue_type_ids.blank?
    joins(:venue_type)
    .where("venue_types.id in (?)", venue_type_ids)
  }
end
