class Package < ActiveRecord::Base
  attr_accessible :description, :image, :name, :price_per_pax
end
