class VenueType < ActiveRecord::Base
  attr_accessible :short, :locale_file_index
  has_many :venues

  def name
    I18n.t(locale_file_index)
  end
end
