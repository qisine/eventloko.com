#encoding: utf-8

module ApplicationHelper

  FLASH_ALIASES = HashWithIndifferentAccess.new(
    error: :error, success: :success,
    alert: :alert, warning: :alert, warn: :alert,
    info:  :info,  notice: :info
  )
  
  # for each record in the rails flash this
  # function wraps the message in an appropriate div
  # and displays it
  def flash_display(clazz = '', id = 'flashes')
    content_tag(:div, flashes, class: clazz, id: id)
  end
  
  def flashes
    flash.select{|key, msg| msg.present?}.collect { |key, msg|
      content = '<a class="close" data-dismiss="alert">×</a> '.html_safe + msg
      content_tag(:div, content, class: "alert alert-#{FLASH_ALIASES[key]}")
    }.join.html_safe
  end

  def error_messages_for(resource)
    return nil if resource.errors.empty?

    error_hash = {}
    error_hash[:messages] = resource.errors.full_messages 
    error_hash[:sentence] = I18n.t("errors.messages.not_saved",
      :count => resource.errors.count,
      :resource => resource.class.model_name.human.downcase)

    render partial: "shared/error_messages", locals: { error_hash: error_hash}
  end
end
