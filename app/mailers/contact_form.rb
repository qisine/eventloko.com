class ContactForm < MailForm::Base
  attribute :name, :validate => true
  attribute :email, :validate => true
  attribute :message, :validate => true
  attribute :file, :attachment => true, :allow_blank => true
  attribute :nickname, :captcha  => true # antispam
  
  attr_accessor :to  
  def headers
    {
      :subject => "#{name} Contact Form",
      :to => "#{@to}",
      :from => %("#{name}" <#{email}>)
    }
  end
end
