class Owners::VenuesController < OwnersController
  authorize_resource

  def index
    @venues = venues_accessible_by_current_ability

    respond_to do |r|
      r.html { }
    end
  end

  def show
    @venue = venues_accessible_by_current_ability.find(params[:id])
    authorize! :read, @venue

    respond_to do |r|
      if @venue
        r.html { }
      else
        r.html { redirect_to :back, alert: t("global.management.no_such_record") }
      end
    end
  end

  def new
    @venue = Venue.new
    @venue.build_address
    @venue.build_pricing
  end

  def edit
    @venue = venues_accessible_by_current_ability.find(params[:id])
    authorize! :edit, @venue

    respond_to do |r|
      if @venue
        #@gallery_images = @venue.gallery_images
        r.html { }
      else
        r.html { redirect_to :back, notice: t("global.management.no_such_record") }
      end
    end
  end

  def create
    @venue = Venue.new(params[:venue])
    @venue.owner_id = current_owner.id

    respond_to do |r|
      if @venue.save
        flash[:notice] = t("global.management.creation_successful") 
        r.html { redirect_to action: 'show', id: @venue.id }
      else
        flash.now[:error] = t("global.management.error_during_processing")
        r.html { render action: :new }
      end
    end
  end

  def update
    @venue = venues_accessible_by_current_ability.find(params[:id])
    authorize! :edit, @venue

    respond_to do |r|
      if @venue.update_attributes(params[:venue])
        flash[:notice] = t("global.management.update_successful") 
        r.html { redirect_to action: 'show', id: @venue.id }
      else
        flash.now[:error] = t("global.management.error_during_processing")
        r.html { render action: :edit }
      end
    end
  end

  def destroy
    @venue = venues_accessible_by_current_ability.find(params[:id])
    authorize! :destroy, @venue

    respond_to do |r|
      if @venue.try(:destroy)
        flash[:notice] = t("global.management.deletion_successful") 
        r.html { redirect_to action: 'index' }
      else
        flash.now[:error] = t("global.management.error_during_processing")
        r.html { render action: :index }
      end
    end
  end

  protected

  def venues_accessible_by_current_ability
    Venue.accessible_by current_ability
  end
end
