class Owners::EventsController < OwnersController
  authorize_resource

  respond_to :js, only: [:new, :edit, :create, :update, :destroy]
  respond_to :json, only: [:index, :update, :destroy]

  def index
    respond_to do |format|
      format.html
      format.json do
        raise :error if params['start'].blank? || params['end'].blank?
        @start = Time.at(params['start'].to_i).to_date
        @end = Time.at(params['end'].to_i).to_date
        @events = events_accessible_by_current_ability.in(@start, @end)
        @once = @events.find_all { |a| a.repeats == 'no' }
        @daily = @events.find_all { |a| a.repeats == 'daily' }
        @weekly = @events.find_all { |a| a.repeats == 'weekly' }
        render 'public/events/index'
      end
    end
  end

  def edit
    @event = events_accessible_by_current_ability.find(params[:id])
    @venues = Venue.accessible_by current_ability
    authorize! :edit, @event

    respond_with @event
  end

  def new
    @event = Event.new(parse_dates(params[:event]))
    @event.repeats = "no"
    @venues = Venue.accessible_by current_ability

    respond_with @event 
  end

  def create
    @event = Event.new(parse_dates(params[:event]))
    @venues = Venue.accessible_by current_ability

    if @event.valid?
      authorize! :create, @event
      @event.save!
    end

    respond_with @event
  end

  def update
    @event = events_accessible_by_current_ability.find(params[:id], readonly: false)
    @venues = Venue.accessible_by current_ability
    authorize! :update, @event

    @event.update_attributes!(parse_dates(params[:event]))
    respond_with @event
  end

  def destroy
    @event = events_accessible_by_current_ability.find(params[:id])
    authorize! :destroy, @event
    @event.destroy
    respond_with @event
  end

  protected

  def events_accessible_by_current_ability
    Event.accessible_by current_ability
  end

  # time is represented in local time, and we remove the offset
  # so it does not jump up (because of the offset) in the ui
  def parse_dates(event_hash)
    if event_hash[:start_at].present?
      t = DateTime.parse(event_hash[:start_at])
      event_hash[:start_at] = (t.utc + t.offset).to_formatted_s(:db)
    end
    if event_hash[:end_at].present?
      t = DateTime.parse(event_hash[:end_at])
      event_hash[:end_at] = (t.utc + t.offset).to_formatted_s(:db)
    end
    event_hash
  end
end
