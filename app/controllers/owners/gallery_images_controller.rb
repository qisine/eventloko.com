class Owners::GalleryImagesController < OwnersController
  authorize_resource

  def create
    #require 'debugger'; debugger
    success = false
    params[:venue][:gallery_images_attributes].each do |k, v|
      image = GalleryImage.new(v)
      next if image.image.blank?
      image.venue_id = params[:venue][:venue_id]
      authorize! :create, image
      success = image.save 
    end
      
    type, message = nil, nil
    if(success)
      type, message = :notice, t("global.management.creation_successful")
    else
      type, message = :error, t("global.management.error_during_processing")
    end

    if request.xhr?
      flash.now[type] = message 
    else
      flash[type] = message
    end

    respond_to do |r|
      r.js 
      r.html {
        redirect_to :back if !request.xhr?
      }
    end
  end

  def destroy
    @image = GalleryImage.find(params[:id])
    authorize! :destroy, @image

    type, message = nil, nil
    if(@image.destroy)
      type, message = :notice, t("global.management.deletion_successful")
    else
      type, message = :error, t("global.management.error_during_processing")
    end

    respond_to do |r|
      r.js { flash.now[type] = message }
      r.html {
        flash[type] = message
        redirect_to :back 
      }
    end
  end
end
