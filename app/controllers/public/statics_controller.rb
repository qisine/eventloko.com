class Public::StaticsController < PublicController
  def show
    render template: "public/statics/#{params[:id]}"
  end  
end
