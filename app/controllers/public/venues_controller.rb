class Public::VenuesController < ApplicationController
  respond_to :js, :html, :json #should use new respond_with

  def index
    @search = { page: params[:page] || 1 }
    @venue_types = VenueType.joins(:venues).all.uniq {|vt| vt.locale_file_index }
    @venues = Venue.order(:name).page(@search[:page])
    respond_to do |r|
      r.html { contact_form }
    end
  end

  def show
    @venue = Venue.find(params[:id])
  end

  def search
    @search = params[:search].try(:dup) || {}
    @search[:page] = params[:page] || 1
    @search[:event_date] =
      Date.parse(@search[:event_date]).strftime("%d-%m-%Y") unless @search[:event_date].blank?

    @venue_types = VenueType.joins(:venues).all.uniq {|vt| vt.locale_file_index }

    @venues = Venue.address_search(@search[:location])
                .date_search(@search[:event_date])
                .price_search(@search[:price_low], @search[:price_high])
                .venue_type_search(@search[:venue_types])
                .order(:name)
                .uniq { |v| v.id }
                .page(@search[:page])

    respond_to do |r|
      r.html { contact_form }
    end
  end

  def contact
    contact_form = ContactForm.new(params[:contact_form])
    contact_form.to = Venue.find(params[:id]).owner.try(:email)
    success = !contact_form.to.blank? && contact_form.deliver

    type, message = nil, nil
    if success
      type, message = :notice, t("global.contact_form.submission_successful")
    else
      type, message = :error, t("global.contact_form.submission_error") 
    end

    respond_to do |r|
      r.js {
        flash.now[type] = message
      }
      r.html {
        flash[type] = message
        redirect_to :back
      }
    end
  end

  def availability
    v = Venue.find(params[:id])
    @occupied_dates = v.try { |v| v.events.pluck(:start_at).map{ |d| d.strftime("%Y-%m-%d") } } || []
    respond_to do |r|
      r.json { render json: @occupied_dates.to_json }
    end
  end

  protected

  def contact_form
    init = customer_signed_in? ? { name: current_customer.name, email: current_customer.email } : {}
    @contact_form = ContactForm.new(init)
  end
end
