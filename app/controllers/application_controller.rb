class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :set_locale

  def set_locale
    I18n.locale = params[:locale].blank? ? nil : params[:locale]
    I18n.locale ||=
      current_app_user.try { |u| u.default_locale.blank? ? nil : current_app_user.default_locale }
    I18n.locale ||= I18n.default_locale
    sync_app_user_locale
  end 

  def sync_app_user_locale
    if current_app_user && I18n.locale != current_app_user.default_locale 
      current_app_user.update_attribute(:default_locale, I18n.locale) 
    end
  end

  def current_app_user
    current_owner || current_customer
  end

  def current_ability
    @current_ability ||=
      current_owner ? ::OwnerAbility.new(current_owner) : ::CustomerAbility.new(current_customer)
  end
end
