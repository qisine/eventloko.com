namespace :db do
  desc 'Create Test Data'
  task :test_data => :seed do
    f = File.join(Rails.root, "db", "test_data.rb")
    load(f) if File.exist?(f)
  end
end
