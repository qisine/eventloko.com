Eventutti::Application.routes.draw do
  scope "(:locale)", :locale => /en|de/ do
    devise_for :owners
    devise_for :customers
    devise_for :users, class_name: 'Customer', path: 'customers'

    namespace :public do
      resources :statics, id: /(about|help|contact|terms)/, only: :show
      resources :home, only: :index
      resources :venues, only: [:index, :show] do 
        get 'search/(:page)', action: 'search', on: :collection, as: 'search'
        get 'availability', action: 'availability', on: :member, as: 'availability'
        post 'contact/:id', action: 'contact', on: :collection, as: 'contact'
      end
    end

    namespace :owners do
      resources :venues do
        resources :events
        resources :gallery_images, only: [:create, :destroy]
      end
      resources :gallery_images, only: [:create, :destroy]
      resources :events, only: [:new, :index, :create, :edit, :update, :destroy]
    end
  end

  root to: 'public/home#index'
end
