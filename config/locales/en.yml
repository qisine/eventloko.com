en:
  global:
    venue_search:
      where: "Where?"
      when: "When?"
      submit: "Go!"
      price_low: "From"
      price_high: "To"
      view_details: "View Details"
      enquire_now: "Enquire Now"
      starting_from: "From"
      placeholders:
        venue_type: "e.g. 'Restaurant' or 'Dance Club'"
        location: "e.g. Zurich"
        event_date: "e.g. 24/12/2013"

    venue_details:
      overview: "Overview"
      availability: "Availability"
      contact: "Contact"

    venue_types:
      restaurant: "Restaurant"
      bar: "Bar"
      meeting_room: "Meeting Room"
      mansion: "Mansion"
      kitchen: "Event Kitchen"

    devise:
      sign_in: "Sign in"
      sign_up: "Sign up"
      sign_out: "Sign out"
      forgot_passwd: "Forgot password?"
      edit_settings: "Edit settings"
      leave_blank_if_no_change: "Leave blank if you don't want to change it"
      new_password: "New password"
      send_reset_instructions: "Please reset my password!"
      
    statics:
      contact: "Contact"
      help: "Help"
      about: "About"
      terms: "Terms & Conditions" 
      texts:
        about: "Eventutti.ch is a platform that allows users to find event venues. The goal is to make booking an event location as hassle free as booking a hotel room. Eventutti is still in the early launch phase, and many features are still being worked on. So, stayed tuned!"
        contact: "If you have any questions or concerns, please contact us anytime at <b>%{email}</b>. We will respond to your enquiry as soon as possible."
        terms:
          "1": "Eventutti.ch is a service <b>free of charge</b> for regular users (as opposed to owners)."
          "2": "Our service is provided <b>'as is'</b> and does not come with <b>any warranty</b> whatsoever."
          "3": "In case of legal proceedings, the jurisdiction is <b>8800 Thalwil</b>, Switzerland. The applicable law is <b>Swiss law</b>."

    contact_form:
      your_name: "Your name"
      your_email: "Your email"
      desired_date: "Desired date"
      optional_file: "Optional file"
      message: "Message"
      submission_successful: "Message successfully submitted!"
      submission_error: "Error during submission!"

    actions:
      edit: "Edit"
      show: "Show"
      delete: "Delete"
      submit: "Submit"
      create: "Create"
      update: "Update"
      new: "New"
      save: "Save"
      cancel: "Cancel"
      back: "Back"
      back_to_overview: "Back to Overview"
      back_to_home: "Back to Home"
      venue:
        new: "New Venue"
        manage: "Manage Venues"
      image:
        edit: "Edit Images"
      event:
        manage: "Manage Events"

    management:
      update_successful: "Update successful"
      creation_successful: "Creation successful"
      deletion_successful: "Deletion successful"
      error_during_processing: "Oops! Something went awry!"

    misc:
      owners: "Owners"
      customers: "Customers"
      are_you_sure: "Are you sure?"
      error: "Error!"
      images: "Images"

    upload_form:
      zero_images: "No images so far :("
      uploaded_images: "Uploaded Images"
      new_image: "New Image"

    "calendar":
      all_day: "All day"
      repeats:
        "no": "Does not repeat"
        daily: "Repeats daily"
        weekly: "Repeats weekly"

  # Tools
  ##########################################################

  views:
    pagination:
      first: "&laquo; First"
      last: "Last &raquo;"
      previous: "&lsaquo; Prev"
      next: "Next &rsaquo;"
      truncate: "..."

  simple_form:
    error_notification:
      default_message: "Oops! There were some errors..."

  # Errors
  ##########################################################

  errors:
    # The default format to use in full error messages.
    format: "%{attribute} %{message}"

    # The values :model, :attribute and :value are always available for interpolation
    # The value :count is available when applicable. Can be used for pluralization.
    messages:
      inclusion: "is not included in the list"
      exclusion: "is reserved"
      invalid: "is invalid"
      confirmation: "doesn't match confirmation"
      accepted: "must be accepted"
      empty: "can't be empty"
      blank: "can't be blank"
      too_long: "is too long (maximum is %{count} characters)"
      too_short: "is too short (minimum is %{count} characters)"
      wrong_length: "is the wrong length (should be %{count} characters)"
      not_a_number: "is not a number"
      not_an_integer: "must be an integer"
      greater_than: "must be greater than %{count}"
      greater_than_or_equal_to: "must be greater than or equal to %{count}"
      equal_to: "must be equal to %{count}"
      less_than: "must be less than %{count}"
      less_than_or_equal_to: "must be less than or equal to %{count}"
      odd: "must be odd"
      even: "must be even"
      at_least_one: "must be at least one"

  # ActiveRecord
  ##########################################################

  activerecord:
    errors:
      messages:
        taken: "has already been taken"
        record_invalid: "Validation failed: %{errors}"
        one_translation_per_locale: "must have at most one translation per locale"
        start_smaller_than_end: "start should be smaller than end"
        start_same_date_end: "start and end should be in the same date"
        
    models:
      customer: "Customer"
      owner: "Owner"
      address: "Address"
      venue: "Venue"
      venue_type: "Venue Type"
      gallery_image: "Gallery Images"
      pricing: "Rental pricing"
       
    attributes:
      customer:
        email: "Email"
        first_name: "First name"
        last_name: "Last name"
        company: "Company"
        password: "Password"
        password_confirmation: "Password confirmation"
        current_password: "Current password"
        remember_me: "Remember me"
        default_locale: "Preferred language"
        
      owner:
        email: "Email"
        name: "Last name"
        venues: "Venues"
        password: "Password"
        password_confirmation: "Password confirmation"
        current_password: "Current password"
        remember_me: "Remember me"
        default_locale: "Preferred language"
        
      venue:
        name: "Name"
        capacity: "Capacity"
        description: "Description"
        size: "Size (sqm)"
        default_venue: "Default venue"
        main_image: "Main Image"
        venue_type: "Venue Type"

      address:
        street: "Street"
        city: "City"
        zip: "ZIP Code"
        directions: "Directions"
      
      gallery_image:
        image: "Image"
        description: "Description"

      event:
        name: "Name"
        start_at: "Starts at"
        end_at: "Ends at"
        all_day: "All day"
        repeats: "Repeats"

      pricing:
        full_day: "Full day"
        deposit: "Deposit"

  # Numbers
  ##########################################################
  
  number:
    # Used in number_with_delimiter()
    # These are also the defaults for 'currency', 'percentage', 'precision', and 'human'
    format:
      # Sets the separator between the units, for more precision (e.g. 1.0 / 2.0 == 0.5)
      separator: "."
      # Delimits thousands (e.g. 1,000,000 is a million) (always in groups of three)
      delimiter: ","
      # Number of decimals, behind the separator (the number 1 with a precision of 2 gives: 1.00)
      precision: 3
      # If set to true, precision will mean the number of significant digits instead
      # of the number of decimal digits (1234 with precision 2 becomes 1200, 1.23543 becomes 1.2)
      significant: false
      # If set, the zeros after the decimal separator will always be stripped (eg.: 1.200 will be 1.2)
      strip_insignificant_zeros: false

    # Used in number_to_currency()
    currency:
      format:
        # Where is the currency sign? %u is the currency unit, %n the number (default: $5.00)
        format: "%u%n"
        unit: "$"
        # These five are to override number.format and are optional
        separator: "."
        delimiter: ","
        precision: 2
        significant: false
        strip_insignificant_zeros: false

    # Used in number_to_percentage()
    percentage:
      format:
        # These five are to override number.format and are optional
        # separator:
        delimiter: ""
        # precision:
        # significant: false
        # strip_insignificant_zeros: false

    # Used in number_to_precision()
    precision:
      format:
        # These five are to override number.format and are optional
        # separator:
        delimiter: ""
        # precision:
        # significant: false
        # strip_insignificant_zeros: false

    # Used in number_to_human_size() and number_to_human()
    human:
      format:
        # These five are to override number.format and are optional
        # separator:
        delimiter: ""
        precision: 3
        significant: true
        strip_insignificant_zeros: true
      # Used in number_to_human_size()
      storage_units:
        # Storage units output formatting.
        # %u is the storage unit, %n is the number (default: 2 MB)
        format: "%n %u"
        units:
          byte:
            one:   "Byte"
            other: "Bytes"
          kb: "KB"
          mb: "MB"
          gb: "GB"
          tb: "TB"
      # Used in number_to_human()
      decimal_units:
        format: "%n %u"
        # Decimal units output formatting
        # By default we will only quantify some of the exponents
        # but the commented ones might be defined or overridden
        # by the user.
        units:
          # femto: Quadrillionth
          # pico: Trillionth
          # nano: Billionth
          # micro: Millionth
          # mili: Thousandth
          # centi: Hundredth
          # deci: Tenth
          unit: ""
          # ten:
          #   one: Ten
          #   other: Tens
          # hundred: Hundred
          thousand: Thousand
          million: Million
          billion: Billion
          trillion: Trillion
          quadrillion: Quadrillion


  # DateTime
  ##########################################################

  # Used in distance_of_time_in_words(), distance_of_time_in_words_to_now(), time_ago_in_words()
  datetime:
    distance_in_words:
      half_a_minute: "half a minute"
      less_than_x_seconds:
        one:   "less than 1 second"
        other: "less than %{count} seconds"
      x_seconds:
        one:   "1 second"
        other: "%{count} seconds"
      less_than_x_minutes:
        one:   "less than a minute"
        other: "less than %{count} minutes"
      x_minutes:
        one:   "1 minute"
        other: "%{count} minutes"
      about_x_hours:
        one:   "about 1 hour"
        other: "about %{count} hours"
      x_days:
        one:   "1 day"
        other: "%{count} days"
      about_x_months:
        one:   "about 1 month"
        other: "about %{count} months"
      x_months:
        one:   "1 month"
        other: "%{count} months"
      about_x_years:
        one:   "about 1 year"
        other: "about %{count} years"
      over_x_years:
        one:   "over 1 year"
        other: "over %{count} years"
      almost_x_years:
        one:   "almost 1 year"
        other: "almost %{count} years"
    prompts:
      year:   "Year"
      month:  "Month"
      day:    "Day"
      hour:   "Hour"
      minute: "Minute"
      second: "Seconds"

  # Helpers
  ##########################################################

  helpers:
    select:
      # Default value for :prompt => true in FormOptionsHelper
      prompt: "Please select"

    # Default translation keys for submit FormHelper
    submit:
      create: 'Create %{model}'
      update: 'Update %{model}'
      submit: 'Save %{model}'

    # Default translation keys for button FormHelper
    button:
      create: 'Create %{model}'
      update: 'Update %{model}'
      submit: 'Save %{model}'


  # Date
  ##########################################################

  date:
    formats:
      # Use the strftime parameters for formats.
      # When no format has been given, it uses default.
      # You can provide other formats here if you like!
      default: "%Y-%m-%d"
      short: "%b %d"
      long: "%B %d, %Y"

    day_names: [Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday]
    abbr_day_names: [Sun, Mon, Tue, Wed, Thu, Fri, Sat]

    # Don't forget the nil at the beginning; there's no such thing as a 0th month
    month_names: [~, January, February, March, April, May, June, July, August, September, October, November, December]
    abbr_month_names: [~, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec]
    # Used in date_select and datetime_select.
    order:
      - :year
      - :month
      - :day

    names:
      today: "Today"
      month: "Month"
      week: "Week"
      day: "Day"

  # Time
  ##########################################################

  time:
    formats:
      default: "%a, %d %b %Y %H:%M:%S"
      short: "%d %b %H:%M"
      long: "%B %d, %Y %H:%M"
      time_only: "%H:%M"
    am: "am"
    pm: "pm"

  # Support
  ##########################################################

# Used in array.to_sentence.
  support:
    array:
      words_connector: ", "
      two_words_connector: " and "
      last_word_connector: ", and "
