# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :proposal_item do
    name "MyString"
    description "MyText"
    number 1
    price 1
    proposal nil
  end
end
