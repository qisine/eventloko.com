# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :package do
    name "MyString"
    description "MyString"
    price_per_pax 1
    image "MyString"
  end
end
