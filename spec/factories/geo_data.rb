# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :geo_datum, :class => 'GeoData' do
    zip 1
    city "MyString"
    city_transliterated "MyString"
    canton "MyString"
  end
end
