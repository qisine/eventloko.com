FactoryGirl.define do
  factory :venue do
    name { Faker::Lorem.sentence(Random.new.rand(3..6)) }
    description { Faker::Lorem.paragraphs(Random.new.rand(2..5)).join("\n") }
    capacity { Random.new.rand(10..100) }
    size { Random.new.rand(30..300) }
    main_image { ImageFixture.pick_venue }
    venue_type { VenueType.find(VenueType.pluck(:id).sample) }
    address
    pricing

    after(:create) do |venue, evaluator|
      FactoryGirl.create_list(:gallery_image, Random.new.rand(3..8), venue: venue)
      FactoryGirl.create_list(:event, Random.new.rand(5..10), venue: venue)
    end
  end
end
