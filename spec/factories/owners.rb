# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :owner do
    email
    address
    password
    password_confirmation {password}

    name { Faker::Company.name }
    phone { Faker::PhoneNumber.phone_number }
    logo { ImageFixture.pick_logo }
    default_locale { ["de", "en"].sample }
  end
end
