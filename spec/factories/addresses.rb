# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :address do
    street { Faker::Address.street_name }
    city { AddressFixture.pick_city } 
    zip { AddressFixture.pick_zip } 
    directions { Faker::Lorem.paragraph(1) }
  end
end
