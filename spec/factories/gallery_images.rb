FactoryGirl.define do
  factory :gallery_image do
    image { ImageFixture.pick_venue }
    description { Faker::Lorem.sentence(Random.new.rand(3..6)) }
  end
end
