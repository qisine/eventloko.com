# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :event do
    pax { Random.new.rand(10..300) }
    all_day { false }
    repeats { "no" }
    customer { Customer.find(Customer.pluck(:id).sample) }

    after(:build) do |event, evaluator|
      event.start_at = Random.new.rand(-500..1000).hours.from_now.beginning_of_day 
                       + Random.new.rand(8..20).hours
      event.end_at = event.start_at + Random.new.rand(1..4).hours
    end
  end
end
