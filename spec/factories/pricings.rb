# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :pricing do
    min, max = 300, 3000
    half_day { Random.new.rand((min/2)..(max/2)) }
    full_day { Random.new.rand(min..max) }
    deposit { Random.new.rand((min/5)..(max/5)) }
  end
end
