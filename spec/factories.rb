Faker::Config.locale = "de"

class ImageFixture
  @@owner_images = (1..5).map { |i| 
    File.open(Rails.root.join('spec', 'fixtures', 'owners', "logo#{i}.jpg").to_s)
  } #+ [[nil, nil]]

  def self.pick_logo;@@owner_images.sample;end

  @@venue_images = (1..5).map { |i| 
    File.open(Rails.root.join('spec', 'fixtures', 'venues', "venue#{i}.jpg").to_s)
  } #+ [[nil, nil]]

  def self.pick_venue;@@venue_images.sample;end
end

class AddressFixture
  @@zips = IO.read(Rails.root.join('spec', 'fixtures', 'addresses', "zips.txt").to_s).split("\n") + [nil, ""]
  @@cities = IO.read(Rails.root.join('spec', 'fixtures', 'addresses', "cities.txt").to_s).split("\n") + [nil, ""]
  def self.pick_zip;@@zips.sample;end
  def self.pick_city;@@cities.sample;end
end

FactoryGirl.define do
  sequence(:email){|n| "#{Faker::Internet.user_name}#{n}@example.com"}
  sequence(:password){|n| "passwd#{n}"}
end
